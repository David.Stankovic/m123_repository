# Fileshare 
- [Fileshare](#fileshare-aufträge)
    - [Auftrag 1](#auftrag-1)
    - [Auftrag 2](#auftrag-2)

# Fileshare Aufträge

## Auftrag 1 Samba

# DNS-Server Dokumentation

Author: **David Stankovic** <br>
Updated Date: **16.01.2023**

# Inhaltsverzeichnis

- [1. Einführung \& Erklärung](#1-einführung--erklärung)
  - [1.1. Einführung](#11-einführung)
  - [1.2. SAMBA](#12-samba)
- [2. Konfiguration eines Sambas](#2-konfiguration-eines-sambas)
- [3. Testing](#3-testing)

# 1. Einführung & Erklärung
## 1.1. Einführung

Aufgabe: Einrichten einen Ubuntu mittels einer VM bei dem man einen Samba-Fileshare Dienst konfigurieren soll.

## 1.2 SAMBA

Samba ist ein Open-Source-Protokoll, das die Datei- und Druckerfreigabe zwischen verschiedenen Betriebssystemen in einem Netzwerk ermöglicht, insbesondere zwischen Linux und Windows. Zusätzlich zu seiner Funktion als Datei- und Druckerdienst kann Samba auch als Domänencontroller in Windows-Netzwerken fungieren. <br>

# 2 Konfiguration eines SAMBAS

Jetzt folgen Installationsschritte zum Einrichten des SAMBA-Fileshare Dienstes:

- **1**. Dieser Schritt ist nicht unbedingt erforderlich. Führe jedoch den Befehl ```sudo apt update``` aus, um den Cache des Projektarchivs neu zu erstellen und sicherzustellen, dass alle installierten Pakete auf dem neuesten Stand sind. <br>
  ➜ <img src="Fileshare/Images/40.png" alt="" width="300"/>
  

- **2**. Den Befehl ```sudo apt install samba``` ausführen. <br>
  ➜ <img src="Fileshare/Images/41.png" alt="" width="300"/>

- **3**. Den Befehl ```sudo systemctl status smbd``` ausführen. <br>
  ➜ <img src="Fileshare/Images/42.png" alt="" width="300"/>


- **4**.   Den Befehl ```sudo systemctl enable- -now smbd``` ausführen.<br>
  ➜  <img src="Fileshare/Images/43.png" alt="" width="300"/>

- **5**.   Den Befehl ```sudo ufw allow samba``` ausführen. <br>
  ➜ <img src="Fileshare/Images/44.png" alt="" width="300"/>

- **6**.  Den Befehl ```sudo usermod -aG sambashare $USER``` ausführen. <br>
  ➜ <img src="Fileshare/Images/45.png" alt="" width="300"/> <br>

- **7**. Dateien öffnen und bei einem Folder Rechtsklick machen, dann auf Eigenschaften. <br>
  ➜ <img src="Fileshare/Images/46.png" alt="" width="300"/> <br>

- **8**. Gleiche Änderungen wie bei nachfolgendem Bild gezeigt duchführen. <br>
  ➜ <img src="Fileshare/Images/47.png" alt="" width="300"/> <br>

- **9**.  Den Befehl ```sudo nano /etc/samba/smb.conf``` ausführen und dort Änderungen wie in den nachfolgenden Bildern vornehmen: <br>
  ➜ <img src="Fileshare/Images/48.png" alt="" width="300"/> <br>
    ➜ <img src="Fileshare/Images/49.png" alt="" width="300"/> <br>

- **10**. Den geteilten Folder öffnen und dort einen neuen Folder erstellen <br>
  ➜ <img src="Fileshare/Images/50.png" alt="" width="300"/> <br>
    ➜ <img src="Fileshare/Images/51.png" alt="" width="300"/> <br>

- **11**. Windows 10 VM aufstarten und Bilder folgen. <br>
  ➜ <img src="Fileshare/Images/52.png" alt="" width="300"/> <br>
    ➜ <img src="Fileshare/Images/53.png" alt="" width="300"/> <br>

# 3. Testing

 Wie man auf den nachfolgenden Bildern sehen kann hat alles geklappt. <br>
  ➜ <img src="Fileshare/Images/54.png" alt="" width="300"/> <br>
    ➜ <img src="Fileshare/Images/55.png" alt="" width="300"/> <br>
      ➜ <img src="Fileshare/Images/56.png" alt="" width="300"/> <br>
 

## Auftrag 2
test