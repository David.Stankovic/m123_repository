# Fileshare 
- [Fileshare](#fileshare-aufträge)
    - [Auftrag 1](#auftrag-1)

# Fileshare Aufträge

## Auftrag 1 

# Drucker Dokumentation

Author: **David Stankovic** <br>
Date: **23.01.2023**

# Inhaltsverzeichnis

- [1. Einführung \& Erklärung](#1-einführung--erklärung)
  - [1.1. Einführung](#11-einführung)
- [2. Prozess](#2-prozess)
- [3. Testing](#3-testing)

# 1. Einführung & Erklärung
## 1.1. Einführung

Aufgabe: Erfolgreich mit dem Client den Drucker verwenden und einen Test ausdrucken.<br>

# 2 Prozess

- **1**. Auf Manage, dann Add Roles and Features, Print and Document Services herunterladen. <br>
  

- **2**. Auf Tools, dort dann Print Management anklicken.

- **3**. Nun einfach dem Bild unten folgen, dann Rechtsklick und Add Printers <br>
  ➜ <img src="Druckerdienste/Images/60.png" alt="" width="300"/>


- **4**.   Durchklicken und dann hier jeweils die IP-Adresse des Druckers eingeben.<br>
  ➜  <img src="Druckerdienste/Images/61.png" alt="" width="300"/>

- **5**.   Als nächstes habe ich ein Text Dokument drucken lassen. <br>


# 3. Testing

 Als nächstes habe ich mit beiden Druckern die Seite ausgedruckt. <br>
   ➜  <img src="Druckerdienste/Images/65.png" alt="" width="300"/>
 