# DHCP-Konfigurationsdokumentation

Author: **David Stankovic** <br>
Updated Date: **18.12.2023**

# Inhaltsverzeichnis

- [DHCP-Konfigurationsdokumentation](#dhcp-konfigurationsdokumentation)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [1. Einführung \& Erklärung](#1-einführung--erklärung)
  - [1.1. Einführung](#11-einführung)
  - [1.2. DHCP](#12-dhcp)
- [2. Logik, Übersicht und Denkweise](#2-logik-übersicht-und-denkweise)
- [3. Installation Ubuntu Server](#4-installation-ubuntu-server)
  - [3.1. Vorkonfiguration des Servers](#41-vorkonfiguration-des-servers)
  - [3.2. Installation des Servers via Terminal](#42-installation-des-servers-via-terminal)
- [4. Troubleshooting](#6-qualified-troubleshooting)

# 1. Einführung & Erklärung
## 1.1. Einführung

Aufgabe: Einrichten eines Ubuntu-Servers mit Installation einer DHCP-Anwendung über das Terminal. Die DHCP-Anwendung soll den DORA-Prozess (Discover, Offer, Request, Acknowledge) mit einem Client ermöglichen. Die Schritte sind in einer groben Dokumentation beschrieben.

## 1.2. DHCP

Ein DHCP-Dienst dient der Automatisierung und Vereinfachung der IP-Adressenvergabe. Insbesondere bei einer Benutzeranzahl von 10 ist die Nutzung eines DHCP-Servers sinnvoll und wird empfohlen. Die Verbindungsschritte und die Kommunikation zwischen DHCP-Server und Client sind im DORA-Prozess (Discover, Offer, Request, Acknowledge) beschrieben. Dieser Prozess umfasst vier wesentliche Schritte:

**DHCPDiscover (Entdecken):** In diesem Schritt erfolgt eine gezielte Suche nach den verschiedenen Netzwerkkomponenten mittels Broadcast.

**DHCPOffer (Angebot):** Der DHCP-Server erhält eine Anfrage zur Vergabe und Reservierung einer IP-Adresse über den Port 67. Er sendet daraufhin die relevanten Daten (Lease-Time, IP, Gateway, DNS) zur Verbindung an den Client zurück, und zwar über Broadcast.

**DHCPRequest (Anforderung):** Der Client akzeptiert das Angebot und meldet dies per Broadcast an den DHCP-Server.

**DHCPAcknowledge (Bestätigung):** Der DHCP-Server empfängt die akzeptierte Anforderung und beginnt mit der Reservierung der IP-Adresse, möglicherweise sogar mit der direkten Vergabe der IP-Adresse.

# 2. Logik, Übersicht und Denkweise

Beginnen wir mit dem DHCP-Server. Dieser verfügt über zwei Netzwerkadapter: Einen für das interne Netzwerk, in dem sich der DHCP-Client, in diesem Fall der DALLUC-Client, befindet, und einen anderen, über den Datenpakete mittels des Befehls "sudo apt" abgerufen werden können.

Der Netzwerkadapter für das **interne** Netzwerk trägt den Namen **Ens33** und hat die Subnetzadresse 192.168.100.0. Über diesen Kanal findet der DORA-Prozess zwischen Client und Server statt.

Der Netzwerkadapter für das **externe** Netzwerk ist als **Ens37** bezeichnet und dient dem Abrufen von Datenpaketen. Die Subnetzadresse in diesem Netz lautet 192.168.98.0.

Der IP-Pool-Bereich für die automatische Vergabe von IP-Adressen wurde angepasst, um den Vorgaben der Sota Handel GmbH zu entsprechen. Der IP-Pool erstreckt sich von 192.168.100.151 bis 192.168.100.200. Alle IP-Adressen für die Sota Handel GmbH befinden sich im gleichen Subnetz, da es nur ein Subnetz gibt.

Der DHCP-Server selbst verwendet die erste Adresse im Pool, nämlich 192.168.100.151. Das Tor zum Netzwerk ist auf 192.168.100.1 festgelegt, von diesem Punkt aus wird die Kommunikation über den Netzwerkadapter Ens33 abgewickelt.

# 3. Installation Ubuntu Server
## 3.1. Vorkonfiguration des Servers

Bevor wir mit dem DHCP-Server anfangen, müssen wir ja noch das Betriebsystem aufsetzen. Mit den folgenden Schritten habe ich das gemacht.

- **1**.	Ich habe die Option «try or install ubuntu server» ausgewählt. <br>

- **2**. Die Schritte danach sind eigentlich selbstverständlich. Man soll sich nur durchklicken und die Einstellungen nach Belieben auswählen.

- **3**. So sollte die Storage Configuration ca. so aussehen. <br>
  ➜ <img src="DHCP/Images/Screenshot_Storage_Configuration.png" alt="" width="300"/>

- **4**. Hier die Informationen eingeben. <br>
  ➜ <img src="DHCP/Images/Screenshot_Name_Ubuntu.png" alt="" width="300"/>

## 3.2 Installation des Servers via Terminal

Alle Befehle werden mit Root ausgeführt. Der Root-User ist das Äquivalent zum Administrator von Windows mit allen Ausführ- und Bearbeitungsrechten. Mit sudo su kann man in den Root-Modus wechseln. Man muss aber nicht, man kann weiterhin sudo bei jedem Befehl eingeben. 
<br>
Jetzt folgen weitere Installationsschritte zum Herunterladen und Aufsetzen unseres ISC-DHCP-Servers.

- **1**. Mit diesem Befehl laden wir unseren DHCP-Dienst herunter. <br>
  ```sudo apt install isc-dhcp-server```

- **2**. Nach einer gewissen Zeitspanne wird der DHCP-Dienst auf dem System heruntergeladen sein. Es ist jedoch zu beachten, dass er noch nicht funktionsfähig ist, da er noch nicht konfiguriert wurde. Lassen Sie uns nun nachsehen, ob der Dienst aktiv ist. Mit dem nachfolgenden Befehl überprüfen wir den aktuellen Status des Dienstes. <br>
  ```sudo systemctl status isc-dhcp-server``` <br>

- **3**. Wir wollen jetzt also den DHCP-Server konfigurieren. Zuerst mal ein Backup der Config-Datei. Dies macht man mit: 
  ```sudo cp /etc/dhcp/dhcpd.conf  /etc/dhcp/dhcpd.conf.backup```

- **4**. Mithilfe von ```sudo nano /etc/dhcp/dhcpd.conf``` öffnet man diese zuvor redundant gespeicherte Datei. Das Bild unten ist die geöffnete Konfigurationsdatei, wo wir unsere IP-Adressen, Pools und sonstiges konfigurieren. <br>
  ➜ <img src="DHCP/Images/Screenshot_config_dhcp.png" alt="" width="300"/>

- **5**. Laten wir uns jetzt unseren Adressbereich definieren. Ein wenig nach unten scrollen und die entsprechende Stelle im Bild suchen. Die Subnetz-IP lautet 192.168.100.0, die Netzmaske ist 255.255.255.0. Unser IP-Pool (Bereich) erstreckt sich von 192.168.100.151 bis 192.168.100.200. Für den DNS-Server geben wir eine beliebige Adresse an, ebenso für den Domain-Namen. Direkt unter dem Domain-Namen befindet sich das Gateway, in unserem Fall mit der Adresse 192.168.100.1. Die Broadcast-Adresse und die Leasetime in Minuten werden ebenfalls festgelegt. Damit ist unsere Konfiguration für den DHCP-Server bereits teilweise abgeschlossen. <br>

- **6**. Um jetzt zu bestimmen, dass Ens37, also unser Netzwerkadapter für unser internes Netzwerk als Kanal für den DORA-Prozess wird, müssen wir das ebenfalls konfigurieren. Ich werde die Interface-Datei öffnen. <br>
  ```sudo nano /etc/default/isc-dhcp-server```

- **7**. Bei INTERFACEv4 soll anstatt **eth’0 -> Ens37** stehen. Wir geben vor, dass Ens37 das Netzwerkadapter für die DHCP-Requests ist. <br>
  ➜ <img src="DHCP/Images/Screenshot_interface.png" alt="" width="300"/>

- **8**.In der Menüleiste navigieren wir zu VM und dann zu Einstellungen, wo wir den Netzwerkadapter "Ens33" hinzufügen, um zu überprüfen, ob die VM überhaupt Datenpakete empfangen kann. Wenn gewünscht, kann auch direkt "Ens37" hinzugefügt werden, um die Überprüfung zu überspringen. Beachten Sie jedoch, dass das Hinzufügen von zwei Netzwerkkarten in Ubuntu zu potenziellem Konfliktpotenzial führen kann. <br>

- **9**. Nun binden wir die beiden Netzwerkadapter ein <br>
  ```sudo nano /etc/netplan/00-installer-config.yaml``` <br>
  ➜ <img src="DHCP/Images/Screenshot_kameh.png" alt="" width="300"/> <br>
  Bei **Ens37** geben wir das obere Adressenfeld ein. Wir bestimmen hier, dass unser Server eine statische 
  <br>
Für **Ens33** legen wir lediglich fest, dass die IP-Adresse über DHCP bezogen wird. Das war auch schon der gesamte Konfigurationsschritt. Jetzt haben beide Netzwerkadapter ihre IP-Adressen. Lassen Sie uns kurz überprüfen, indem wir die Befehle **ip a** und **ip r** verwenden. Mit `ip a` werden alle IP-Adressen angezeigt, und mit `ip r` erhalten wir Informationen zu den Gateways. Hier bemerkte ich dann, dass es nicht stimmen konnte. <br>
  ➜ <img src="DHCP/Images/Screenshot_ip_a.png" alt="" width="300"/> <br>

# 4. Troubleshooting

 1. Als ersten Schritt schaute ich dann in den Einstellungen nach, ob alles gut eingestellt wurde und meiner Meinung nach war alles richtig eingestellt. Ich hatte die zweite Netzwerkkarte und eigentlich war alles gut
 2. Danach machte ich einfach einen reboot bei der VM mittels dem Comman **reboot**
 3. Ich machte daraufhin den Befehl **ip a** und bekam folgenden Output und die dritte Netzwerkkarte wurde erkannt: <br>
   ➜ <img src="DHCP/Images/Screenshot_ip_a_2.png" alt="" width="300"/> <br> 

 4. Danach versuchte ich es mit dem Befehl **sudo netplan apply**. Ich bekam folgende Fehlermeldung: <br>
 ➜ <img src="DHCP/Images/Screenshot_ip_a_2.png" alt="" width="300"/> <br>
 5. Nach einer intensiven Internetrecherche und vielem Rumexperimentieren muss ich sagen, dass ich an dieser Stelle nicht weitergekommen bin.

