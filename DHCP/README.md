# DHCP 
- [DHCP](#dhcp-aufträge)
    - [Auftrag 1](#auftrag-1)
    - [Auftrag 2](#auftrag-2)

# DHCP Aufträge

## Auftrag 1
<br>
# DHCP-Dienst anwenden und verstehen
<br>
Als ersten Schritt habe ich die Laptops mit dem DHCP-Server verbunden.
![Verbindungen](DHCP/Images/Screenshot_Connections.png)
<br>
<br>
<br>
Als nächstes habe ich den DHCP-Server konfiguriert.
<br>
![DHCP-Server Konfiguration](DHCP/Images/Screenshot_DHCP_Server_Setup.png)
<br>
Dannach habe ich die 2 Laptops mit dem DHCP-Server eine IP-Adresse zugewiesen.
![Laptops-einstellen](DHCP/Images/Screenshot_Laptop.png)
<br>
Als zweitletzen Schrit testete ich, ob alles funktioniert.
<br>
![Tester](DHCP/Images/Pfeiltaste.png)
<br>
Als letzten Schritt habe ich noch im DHCP-Server eingestellt, dass der letzte Laptop immer die statische IP-Adresse 192.168.0.50 bekommt.
![Statische IP-Adresse](DHCP/Images/Letzter_Laptop.png)


## Auftrag 2

### 1. Router-Konfiguration auslesen 
Für welches Subnetz ist der DHCP Server aktiv?
> 255.255.255.0
<br>
Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)
<br>

| IP-Adresse    | Client-ID/Hardwareadresse | Lease-Ablaufzeit | Typ        |
| ------------- | ------------------------- | ----------------- | ---------- |
| 192.168.32.30 | 0009.7CA6.4CE3            | --               | Automatic  |
| 192.168.32.33 | 00E0.8F4E.65AA            | --               | Automatic  |
| 192.168.32.32 | 0050.0F4E.1D82            | --               | Automatic  |
| 192.168.32.34 | 0007.ECB6.4534            | --               | Automatic  |
| 192.168.32.31 | 0001.632C.3508            | --               | Automatic  |
| 192.168.32.35 | 00D0.BC52.B29B            | --               | Automatic  |

In welchem Range vergibt der DHCP-Server IPv4 Adressen?
> 192.168.32.1 ----- 192.168.32.254
Was hat die Konfiguration ip dhcp excluded-address zur Folge?
> Schliesst IP-Adressen aus
<br>
Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?
<br>

> 154 IP-Adressen

### 2. DORA - DHCP Lease beobachten


Welcher OP-Code hat der DHCP-Offer?

> OP:=0x0000000000000002
<br>
Welcher OP-Code hat der DHCP-Request?

> OP:=0x0000000000000001

Welcher OP-Code hat der DHCP-Acknowledge?

> OP:=0x0000000000000002

An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?

> An Broadcast, IP-Adresse 0.0.0.0 eigentlich

An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?

> An Broadcast

Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?

> Weil noch keine IP-Adresse vorhanden ist.
<br>
Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der 
DHCP-PDUs festgestellt werden?
<br>
<br>
<br>
Welche IPv4-Adresse wird dem Client zugewiesen?

> Weil es über den Broadcast gesendet wird.

![DHCPDISCOVER](DHCP/Images/Screenshot_PDU.png)
![DHCPDISCOVER](DHCP/Images/Screenshot_2023-12-05_093411.png)



### 3. Netzwerk umkonfigurieren


