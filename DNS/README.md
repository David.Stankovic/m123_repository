# DNS 
- [DNS](#dns-aufträge)
    - [Auftrag 1](#auftrag-1)
    - [Auftrag 2](#auftrag-2)

# DNS Aufträge

## Auftrag 1
# DNS-Server Dokumentation

Author: **David Stankovic** <br>
Updated Date: **09.01.2023**

# Inhaltsverzeichnis

- [1. Einführung \& Erklärung](#1-einführung--erklärung)
  - [1.1. Einführung](#11-einführung)
  - [1.2. DNS](#12-dns)
- [2. Installation des DNS-Servers](#42-installation-des-servers-via-terminal)
- [3. Testing](#6-qualified-troubleshooting)

# 1. Einführung & Erklärung
## 1.1. Einführung

Aufgabe: Einrichten eines Windows-Server mittels einer VM bei dem man einen DNS-Server konfigurieren soll.

## 1.2 DNS

Natürlich! DNS ist wie das Telefonbuch des Internets. Es übersetzt menschenfreundliche Webadressen in die IP-Adressen, die Computer verwenden, um miteinander zu kommunizieren, und ermöglicht so, dass wir Websites über Namen statt komplizierter Zahlen erreichen können.<br>

# 2 Installation des DNS-Servers

Um die DNS-Funktionen herunterzuladen, sollte ich zunächst den Server Manager öffnen und die erforderlichen Features installieren. Der Server Manager spielt eine zentrale Rolle in der Serververwaltung und ermöglicht das Management verschiedener Funktionen. Unser Server kann mehrere Rollen gleichzeitig ausführen, was ihn zu einem Multitasking-Server macht, der Aufgaben wie Domain Controller, Active Directory und DNS/DHCP gleichzeitig bewältigen kann.
<br>
Jetzt folgen weitere Installationsschritte zum Einrichten des DNS-Servers:

- **1**. Nun clickt man oben auf Roles and Feautures, DNS auswählen und der Rest sollte mittels den danachfolgenden Bildern selbstverständlich sein. <br>
  ➜ <img src="DNS/Images/5neu.png" alt="" width="300"/>
  ➜ <img src="DNS/Images/6.png" alt="" width="300"/>
  

- **2**. Dannach auf Tools und einfach auf DNS clicken <br>
  ➜ <img src="DNS/Images/7.png" alt="" width="300"/>

- **3**. Rechtsklick auf den PC machen und und auf New Zone klicken <br>
  ➜ <img src="DNS/Images/8.png" alt="" width="300"/>


- **4**. Bei dem meisten einfach durchklicken, bis man bei Zone File ankommt. Dort sollte man dann ganz einfach einen Namen auswählen und einfach weiterklicken.  <br>
  ➜ <img src="DNS/Images/9.png" alt="" width="300"/>

- **5**. Jetzt machen wir genau den gleichen Schritt, nur dass wir jetzt eine Reverse Lookup Zone machen anstatt einer Forward Lookup Zone. Hier jetzt einfach durchklicken, bis man bei Reverse Lookup Zone Name kommt. Dort sollte man dann einfach die Netzwerk-ID eingeben. Nachdem dies erledigt ist einfach durchklicken, bis man beim Ende der Konfiguration der Reverse Lookup Zone ist. <br>
  ➜ <img src="DNS/Images/11.png" alt="" width="300"/>

- **6**. Nun machen wir einen Rechtsklick auf die Forward Lookup Zone, die wir erstellt haben und klicken auf die Option New Host (A or AAAA) <br>
  ➜ <img src="DNS/Images/14.png" alt="" width="300"/> <br>

- **7**. Als nächsten Schritt den Client und den DNS-Server dort hinzufügen. Es sollte bei der Reverse Lookup Zone automatisch erscheinen. <br>
  ➜ <img src="DNS/Images/15.png" alt="" width="300"/> <br>

- **8**. Rechtsklick oben auf den PC machen und auf Properties gehen, dann auf Forwarder und die Adresse 8.8.8.8 eingeben. <br>
  ➜ <img src="DNS/Images/17.png" alt="" width="300"/> <br>
# 3. Testing

 Wie man auf den nachfolgenden Bildern sehen kann hat alles geklappt. <br>
  ➜ <img src="DNS/Images/18.png" alt="" width="300"/> <br>
    ➜ <img src="DNS/Images/19.png" alt="" width="300"/> <br>
      ➜ <img src="DNS/Images/20.png" alt="" width="300"/> <br>
 
## Auftrag 2
test